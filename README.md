# Readme

## Django shopping project

Clone the project `git clone https://gitlab.com/bsesic/djangotutorial.git`

Apply migrations `python manage.py migrate`

Create a superuser `python manage.py createsuperuser` or use the default (admin/admin).

Run the project `python manage.py runserver`.
