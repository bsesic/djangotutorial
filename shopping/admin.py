from django.contrib import admin

# Register your models here.

from .models import Item, Shop

admin.site.register(Item)
admin.site.register(Shop)