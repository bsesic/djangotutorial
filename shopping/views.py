from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Item, Shop


# Create your views here.


def index(request):
    # lädt alle Artikel und damit verbunden alle Läden
    items = Item.objects.all()
    print("Items: ", items)
    context = {'items': items}
    print("Context:", context)
    return render(request=request, template_name='shopping/index.html', context=context)


def add(request):
    # Shows the add template only
    return render(request=request, template_name='shopping/add.html', context={})


def store(request):
    print("Request:", request.POST)
    if request.POST:
        shop = Shop.objects.get(pk=request.POST['shop'])
        item = Item(
            name=request.POST['name'],
            amount=request.POST['amount'],
            shop=shop,
            status=False
        )
        item.save()
    return redirect('index')
