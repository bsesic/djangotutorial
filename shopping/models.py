from django.db import models

# Create your models here.


class Shop(models.Model):
    # Der Laden wo eingekauft wird
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name



class Item(models.Model):
    # Der einzelne Artikel
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255, blank=False)
    status = models.BooleanField() # can be done or not
    amount = models.CharField(max_length=255, blank=True)
    shop = models.ForeignKey(to=Shop, on_delete=models.CASCADE)

    def __str__(self):
        return self.name